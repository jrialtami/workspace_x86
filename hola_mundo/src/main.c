/*==================[inclusions]=============================================*/

#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void) 
{
    printf("Chau mundo! \n\n");

    return 0;
}

/*==================[end of file]============================================*/
