/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/
#define BASE_TIME_1SEG 10
#define M2S(A) BASE_TIME_1SEG*A

#define TOP_TIMEOUT_SELECTION   M2S(30)//30s
#define TIME_TE_EROGATION       M2S(30)//30s
#define TIME_CAFE_EROGATION     M2S(45)//45s
#define TIME_BUZZ_ON            M2S(2)//2s

#define F_10HZ  M2S(1/10) //100ms
#define F_1HZ   M2S(1) //1s

#define WAIT_PAY        0
#define WAIT_SELECTION  1
#define BUSY_EROGACION  3
#define EROGACION_END   4
#define TIMEOUT         5

/*==================[internal data declaration]==============================*/
uint8_t seleccion = 0;
uint16_t timeout = 0;
uint16_t time = 0;
uint16_t timeErogation = 0;
uint16_t timeBuzz = 0;

uint8_t rise_fall = 0;

/*==================[internal functions declaration]=========================*/

uint8_t stateEngine(uint8_t , uint8_t);
void BlinkAction(uint8_t);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
void BlinkAction(uint8_t freq){
    if(time >= freq) {
        if(rise_fall){
            hw_LedOn();
        }
        else {
            hw_LedOff();
        }
        rise_fall = ~rise_fall;
        time = 0;
    }
}

uint8_t stateEngine(uint8_t input, uint8_t state){
    switch(state){
            case WAIT_PAY:
                if(input == INSERT_COIN) {
                    state = WAIT_SELECTION;
                }
            break;
            case WAIT_SELECTION:
                if(input == TE_BUTTON ){
                    state = BUSY_EROGACION;
                    seleccion = input;
                    hw_OutTeOn();
                }
                if(input == CAFE_BUTTON){
                    state = BUSY_EROGACION;
                    seleccion = input;
                    hw_OutCafeOn();
                }
                if(timeout++ >= TOP_TIMEOUT_SELECTION){
                    state = TIMEOUT;
                }
                BlinkAction(F_1HZ);
            break;
            case BUSY_EROGACION:
                if(seleccion == TE_BUTTON && timeErogation >= TIME_TE_EROGATION){
                    state = EROGACION_END;
                    hw_OutTeOff();
                    hw_BuzzOn();
                }
                if(seleccion == CAFE_BUTTON && timeErogation >= TIME_CAFE_EROGATION){
                    state = EROGACION_END;
                    hw_OutCafeOff();
                    hw_BuzzOn();
                }
                BlinkAction(F_10HZ);
                timeErogation++;
            break;
            case EROGACION_END:
                timeErogation = 0;
                if(timeBuzz++ == TIME_BUZZ_ON){
                    hw_BuzzOff();
                    timeBuzz = 0;
                    state = WAIT_PAY;
                }
            break;
            case TIMEOUT:
                timeout = 0;
                hw_payBack();
                state = WAIT_PAY;
            break;
    }
    time++;
    return state;
}


/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint8_t state = 0;
    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        input = hw_LeerEntrada();
        state = stateEngine(input,state);
        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
