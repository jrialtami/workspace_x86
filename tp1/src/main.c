/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/
#define TIME_CLOSE_TOP 50 //50*100ms = 5sec
#define bool uint8_t
#define true 1
#define false 0
/*==================[internal data declaration]==============================*/
typedef uint8_t time;
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    bool barreraAbierta = false;
    bool contar = false;
    bool sonando = false;
    uint8_t input;
    time t = 0;
    hw_Init();
    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == SENSOR_1 ) {
            if(!barreraAbierta){
                hw_AbrirBarrera();
                barreraAbierta = true;
            }
            if(contar){
                t = 0;
            }
        }

        if( (input == SENSOR_2)  ){
            if(!barreraAbierta) {
                hw_EncenderAlarma();
                sonando = true;
            }
            else if(!contar){
                contar = true;
            }
            
        } else if(sonando){
            hw_ApagarAlarma();
            sonando = false;
        }
        if(contar) {
            t++;
        }
        if(t >= TIME_CLOSE_TOP){
            hw_CerrarBarrera();
            t = 0;
            contar = barreraAbierta = false;
        }

        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
