/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void hw_OutOn(char *description){
    printf("\nSalida de %s Activada\n",description);
}
void hw_OutOff(char *description){
    printf("\nSalida de %s desactivada\n",description);
}

void hw_OutCafeOn(void){
    hw_OutOn("CAFE");
}

void hw_OutCafeOff(void){
    hw_OutOff("CAFE");
}

void hw_OutTeOn(void){
    hw_OutOn("TE");
}

void hw_OutTeOff(void){
    hw_OutOff("TE");
}

void hw_LedOn(void){
    hw_OutOn("LED");
}
void hw_LedOff(void){
    hw_OutOff("LED");
}
void hw_BuzzOn(void){
    hw_OutOn("BUZZER");
}
void hw_BuzzOff(void){
    hw_OutOff("BUZZER");
}
void hw_payBack(){
    printf("Devolver la ficha");
}


/*==================[end of file]============================================*/
