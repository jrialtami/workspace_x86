#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT            27  // ASCII para la tecla Esc 
#define CAFE_BUTTON     49  // ASCII para la tecla 1 
#define TE_BUTTON       50  // ASCII para la tecla 2 
#define INSERT_COIN     51 // ASCII para la tecla 3

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

void hw_Init(void);
void hw_DeInit(void);
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void hw_OutOn(char *);
void hw_OutOff(char *);

void hw_OutCafeOn(void);
void hw_OutCafeOff(void);
void hw_OutTeOn(void);
void hw_OutTeOff(void);

void hw_LedOn(void);
void hw_LedOff(void);

//void hw_BlinkAction(uint8_t freq);

void hw_BuzzOn(void);
void hw_BuzzOff(void);
void hw_payBack(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
